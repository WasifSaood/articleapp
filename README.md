# MostPopularArticle

A simple app to hit the NY Times Most Popular Articles API and:
* Show a list of articles 
* Shows details when items on the list are tapped. 


We are using the most viewed section of this API.
http://api.nytimes.com/svc/mostpopular/v2/mostviewed/{section}/{period}.json?apikey= sample-key To test this API, 
For testAPI we used 
* all-sections for the section path component in the URL
* 7 for period

This is configurable in Settings.Swift file in Project. 
We used MVVM Design pattern and swift generic approach to develop this application.

We are generating TestCase and Coverage report following tool:
* **XCTestCase and Coverage report.**
* **Fastlane+scan TestCase and Coverage reports.**


## Tools And Resources Used
- [fastlane](https://docs.fastlane.tools/) - The easiest way to automate building and releasing your iOS and Android apps.


# Installation

* Installation by cloning the repository
* Go to directory
* use command + B or Product -> Build to build the project
* It will run the application in the Simulator.
* There is no third party dependency in te code, it should should run the aplication without any intervention
* you should be connected with internet to check the functionality.


## Fastlane

Fastlane is setup on Xcode server and integrated in development project also for following activity (Lane):

* Generating TestCase success and code coverage reports using scan and slather.
* Generating screen shot.


#### Installation

* Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install

```

* Install fastlane using  *gem install fastlane*

* For fastlane test coverage report install scan and slather 
* Install scan using  `gem install scan`

&nbsp; 

### # Fastlane ScreenShot Report:

* Run using Terminal
* Goto project directory $ cd project_directory_path
* Run command  `fastlane screenshots` to capture screen shots.

### # Fastlane + Scan Test Code Coverage Report : 
* Run using Terminal
* Goto project directory $ cd project_directory_path
* Run command  `fastlane tests` to capture screen shots.


## Running The Tests Manually 

Follow the steps to get test case reports:
* Enable coverage Data under test schema section:
* Select the Test Icon by pressing and holding Xcode Run Icon OR press `Command+U`
* In the Project Navigator under Test Navigator tab, check test status and coverage 


#### XCTestCase Code Coverage Report:   _91.1%_

* Total TestCase: 7
* Passed : 7
* Failed: 0
* TestCase Code Coverage: **91.1%**
