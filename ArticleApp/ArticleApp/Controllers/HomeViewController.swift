//
//  ViewController.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    internal let searchController = UISearchController(searchResultsController: nil)
    internal var viewModel = ArtileListViewModel()
    var cache:NSCache<AnyObject, AnyObject>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  !Reachability.Connection(){
            DispatchQueue.main.async {
                self.showAlertMessage(titleStr: "Error", messageStr: "The Internet connection appears to be offline.")
            }
        }
        else{
            observeEvents()
            viewModel.viewDidLoad()
            self.cache = NSCache()
            tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = 200
        }
        
    }
    
    
    
    //MARK: Function to observe event call backs from the viewmodel.
    private func observeEvents() {
        let spinner = showLoader(view: self.view)
        viewModel.reloadTable = { [weak self] (error: ErrorModel?, response: [ArticleList]?)  in
            DispatchQueue.main.async {
                if let error = error{
                    spinner.dismissLoader()
                    self?.showAlertMessage(titleStr: "Error", messageStr: error.fault.faultstring ?? "something went wrong")
                }
                
                self?.tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
                spinner.dismissLoader()
                self?.tableView.reloadData()
            }
        }
    }
}

