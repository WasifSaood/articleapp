
//
//  ArticleDetailVC.swift
//  ArticleApp
//
//  Created by Wasif Saood on 21/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit

class ArticleDetailVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var byLineLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    
    internal var artileData: ArticleList? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        downloadImage()

    }
    //MARK: SetUp UI
    private func setUpUI() {
        scrollView.bottomAnchor.constraint(equalTo: descriptionLabel.bottomAnchor).isActive = true
        self.byLineLabel.text = artileData?.byline
        self.descriptionLabel.text = artileData?.abstract
        self.titleLabel.text = artileData?.title
        self.dateLabel.text = artileData?.published_date
    }
    //MARK: Download Image
    private func downloadImage(){
        if (artileData?.media[0].media_metadata.count)! > 1 {
        if let url = artileData?.media[0].media_metadata[2].url {
            ImageDownloader.shared.downloadArticleImage(urlString: url) {[weak self] (img: UIImage?, error: Bool?) in
                DispatchQueue.main.async(execute: { () -> Void in
                    if error ?? false{
                        self?.showAlertMessage(titleStr: "Error", messageStr: "Can't download Imge")
                    }
                    else {
                        self?.articleImage?.image = img
                    }

                })
            }
         }
        }
    }
}
