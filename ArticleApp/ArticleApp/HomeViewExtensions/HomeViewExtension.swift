//
//  HomeViewExtension.swift
//  ArticleApp
//
//  Created by Wasif Saood on 23/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit


//MARK: UITableViewDatasource
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as! ArticleTableViewCell
            cell.selectionStyle = .none
            cell.item = viewModel.dataSource[indexPath.row]
            cell.articleImage?.image = UIImage(named: "nyt")
            
            if viewModel.dataSource[indexPath.row].media[0].media_metadata.count > 0{
                let imageURL = viewModel.dataSource[indexPath.row].media[0].media_metadata[0].url
                //Image Downloader And Cache Image
                if (self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
                    cell.articleImage?.image = self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
                }else{
                    ImageDownloader.shared.downloadArticleImage(urlString: imageURL) { [weak self] (img: UIImage?, err: Bool?) in
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let updateCell = tableView.cellForRow(at: indexPath as IndexPath) as? ArticleTableViewCell {
                                if let img = img {
                                    updateCell.articleImage?.image = img
                                     self?.cache.setObject(img, forKey: (indexPath as NSIndexPath).row as AnyObject)
                                }
                            }
                        })
                    }
                }
            }else {
                cell.articleImage?.image = UIImage(named: "nyt")
            }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.navigateToPlaceListWithPlaceType(&viewModel.dataSource[indexPath.row])
    }
}

// MARK: Routing
extension HomeViewController {
    
    private func navigateToPlaceListWithPlaceType(_ articleDetail: inout ArticleList) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
        controller.artileData = articleDetail
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
