//
//  ImageDownloader.swift
//  ArticleApp
//
//  Created by Wasif Saood on 23/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit


class ImageDownloader {
    
    static let shared = ImageDownloader()
    
    func downloadArticleImage(urlString: String, completion: @escaping (UIImage?, _ err: Bool?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to get data:", err)
            }
            if let data = try? Data(contentsOf: url){
                DispatchQueue.main.async(execute: { () -> Void in
                    let img:UIImage! = UIImage(data: data)
                        completion(img, false)
                   
                })
            }
            else{
                completion(nil, true)
            }
            }.resume()
        
    }
    
}
