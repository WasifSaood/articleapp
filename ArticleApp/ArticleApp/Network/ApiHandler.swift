//
//  ApiHandler.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

struct ApiService {

    static let shared = ApiService()

    func fetchApiData(urlString: String, completion: @escaping ([ArticleList]?, ErrorModel?) -> ()) {
        guard let url = URL(string: urlString) else {
            completion(nil, nil)
            return
        }
        print("*************")
        print("Endpoint url: \(url)")
        print("*************")
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let err = err {
                print("Failed to get data:", err)
                return
            }
            if let error = self.checkResponse(response: response, data: data) {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            
            if let responseData: [ArticleList] = self.handleSuccess(data: data) {
                DispatchQueue.main.async {
                    completion(responseData, nil)
                }
            }
            else {
                completion(nil, nil)
            }
        }.resume()

    }

    func handleSuccess(data: Data?) -> [ArticleList]? {
        guard let data = data else { return nil }
        do {
            let responseModel = try JSONDecoder().decode(Articles.self, from: data)
            return responseModel.results
        } catch let jsonErr {
            print("Failed to serialize json:", jsonErr)
        }
        return nil
    }

    func checkResponse(response: URLResponse?, data: Data?) -> ErrorModel? {
        if let httpResponse = response as? HTTPURLResponse {
            if httpResponse.statusCode != 200 {
                let error = self.errorHandle(data: data)
                return error
            }
        }
        return nil
    }

    func errorHandle(data: Data?) -> ErrorModel? {
        var error: ErrorModel?
        guard let data = data else { return nil }
        do {
            error = try JSONDecoder().decode(ErrorModel.self, from: data)
        }
        catch let jsonErr {
            print("Failed to serialize error in json:", jsonErr)
        }
        print("Message : \(error?.fault.faultstring ?? "")")
        return error
    }

}
