//
//  ArtileListViewModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

class ArtileListViewModel {
    
    // MARK: Input
    var viewDidLoad: ()->() = {}
    var dataSource  = [ArticleList]()
    
    // MARK: Events
    var reloadTable: (_ error : ErrorModel?, _ response : [ArticleList]?)->() = { _,_  in }
    
    init() {
        viewDidLoad = { [weak self] in
            let url = Routes().createMostPopulrAPI(period: kPageNumber)
            ApiService.shared.fetchApiData(urlString: url) { (response: [ArticleList]?, error: ErrorModel?) in
                if let error = error {
                    print(error)
                }
                if let response = response {
                    self?.reloadTable(error, response)
                    if response.count > 0 {
                        self?.dataSource.append(contentsOf: response)
                    }
                }
                else {
                    self?.reloadTable(error, nil)
                }
            }
        }
    }
}
