//
//  TableViewCell.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    var item: Any? {
        didSet {
            self.configure(self.item)
        }
    }
    func configure(_ item: Any?) { }
}
