//
//  ArtcileTableViewCell.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit

class ArticleTableViewCell: TableViewCell {


    @IBOutlet weak var byLineLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    override func configure(_ item: Any?) {
        guard let model = item as? ArticleList else { return }
        self.byLineLabel.text = model.byline.trim()
        self.headerLabel.text = model.title.trim()
        self.dateLabel.text = model.published_date
    }
    
}
