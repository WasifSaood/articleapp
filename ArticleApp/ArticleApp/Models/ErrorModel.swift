//
//  ErrorModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
///

import Foundation

struct ErrorModel: Decodable {
    let fault: FaultMessages
}

struct FaultMessages : Decodable{
    let faultstring: String?
}

