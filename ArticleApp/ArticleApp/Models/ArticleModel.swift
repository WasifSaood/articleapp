//
//  ArticleModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

struct Articles: Decodable {
    let results: [ArticleList]
}

//struct Results: Decodable {
//    let results: [ArticleList]
//}

struct ArticleList: Decodable {
    let title: String
    let byline: String
    let published_date: String
    let abstract: String
    let media: [Multimedia]
}

struct Multimedia: Decodable {
    let media_metadata: [Images]
    
    enum CodingKeys: String, Codable, CodingKey {
        case media_metadata = "media-metadata"
    }
}

struct Images: Decodable {
    let url: String
}

