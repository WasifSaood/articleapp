//
//  ArticleAppTests.swift
//  ArticleAppTests
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import XCTest
@testable import ArticleApp

let kPageArticleCount = 1
let kSampleArticleTitle = "Teenager Accused of Rape Deserves Leniency Because He's From a 'Good Family' Judge Says"
let kSampleArticleCount = 1
let kSampleArticlePubDate = "2019-07-02"
let kSampleArticleByline = "By LUIS FERRE-SADURNI"

class ArticleAppTests: XCTestCase {

    var homeViewController: HomeViewController!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        homeViewController = (storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController)
        
        //load view hierarchy
        _ = homeViewController.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testParseArticleErrorData() {
        // giving a sample json file
        guard let data = FileManager.readJson(forResource: "Error") else {
            XCTAssert(false, "Can't get data from Error.json")
            return
        }
        let error = ApiService.shared.errorHandle(data: data)
        XCTAssertNotNil( error )
    }
    
    func testCheckAPIData(){
        let url = Routes().createMostPopulrAPI(period: kPageNumber)
        ApiService.shared.fetchApiData(urlString: url) { (response: [ArticleList]?, error: ErrorModel?) in
            XCTAssertGreaterThan(response?.count ?? 0,kPageArticleCount, "Expected \(kPageArticleCount)")
        }
    }
    
    func testCheckAlert(){
        UIApplication.shared.delegate?.window!?.rootViewController = homeViewController
     UIApplication.shared.delegate?.window!?.rootViewController?.showAlertMessage(titleStr: "Test", messageStr: "Sample")

        XCTAssertTrue((homeViewController.presentedViewController?.isKind(of: UIAlertController.self))!)
    }
    
    
    func testParseArticleMockData() {
        // giving a sample json file
        guard let data = FileManager.readJson(forResource: "Sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }
        
        do {
            let responseModel = try JSONDecoder().decode(Articles.self, from: data)
            XCTAssertEqual(responseModel.results.count, kSampleArticleCount, "Expected \(kSampleArticleCount)")
            XCTAssertEqual(responseModel.results[0].title, kSampleArticleTitle, "Expected \(kSampleArticleTitle)")
            XCTAssertEqual(responseModel.results[0].byline, kSampleArticleByline, "Expected \(kSampleArticleByline)")
            XCTAssertEqual(responseModel.results[0].published_date, kSampleArticlePubDate, "Expected \(kSampleArticlePubDate)")
            
        } catch _ {
            XCTAssert(false, "Expected valid Aricle Data")
        }
    }
    
    func testCheckWrongURL(){
        let url = Routes().createMostPopulrAPI(period: kPageNumber)
        let expectedURL = "https://api.nytimes.com/svc/mostpopular/v2/viewed/\(kPageNumber).json?api-key=AXcQkSoMr748TcN0felPNzG34IM8Gb6I"
        XCTAssertEqual(url, expectedURL)
    }


}


extension FileManager {
    
    static func readJson(forResource fileName: String ) -> Data? {
        
        let bundle = Bundle(for: ArticleAppTests.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                // handle error
            }
        }
        
        return nil
    }
    
}
