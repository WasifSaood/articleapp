//
//  ArticleAppUITests.swift
//  ArticleAppUITests
//
//  Created by Wasif Saood on 20/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import XCTest

class ArticleAppUITests: XCTestCase {
    
    var app = XCUIApplication()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        
        XCUIApplication().launch()
        app = XCUIApplication()
        setupSnapshot(app)
    }
    
    
    func testTableInteraction() {
        // Assert that we are displaying the tableview
        app.launch()
        
        sleep(15)
        let cells = app.tables.cells
        snapshot("App Screenshot Launch with indicator")
        
        if cells.count > 0 {
            snapshot("after load")
            let count: Int = (cells.count - 1)
            
            let promise = expectation(description: "Wait for table cells")
            
            for i in stride(from: 0, to: count , by: 1) {
                let tableCell = cells.element(boundBy: i)
                XCTAssertTrue(tableCell.exists, "The \(i) cell is in place on the table")
                tableCell.tap()
                
                if i == (count - 1) {
                    snapshot("detail View")
                    promise.fulfill()
                }
                
                // Back
                app.navigationBars.buttons.element(boundBy: 0).tap()
            }
            
            waitForExpectations(timeout: 50, handler: nil)
            XCTAssertTrue(true, "Finished validating the table cells")
        } else {
            XCTAssert(false, "Was not able to find any table cells")
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
